package com.example.chat.route

import akka.NotUsed
import akka.http.scaladsl.model.ws.{Message, TextMessage}
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import akka.stream.Materializer
import akka.stream.scaladsl.{BroadcastHub, Flow, Keep, MergeHub, Sink, Source}
import com.example.chat.Protocol.{ChatMessage, UserJoined, UserLeft}
import com.example.chat.{Paths, Protocol}
import io.circe.generic.AutoDerivation
import org.slf4j.LoggerFactory


class WebsocetRoute(implicit materializer: Materializer) extends AutoDerivation {

  private val logger = LoggerFactory.getLogger(getClass)

  def route: Route = path(Paths.ChatSocket) {
    parameter('name) { userName =>
      handleWebSocketMessages(socketFlow(userName))
    }
  }

  import io.circe.syntax._

  val (sink: Sink[Protocol.Message, NotUsed], source: Source[Protocol.Message, NotUsed]) =
    MergeHub.source[Protocol.Message](perProducerBufferSize = 256)
      .map { msg => logger.info(s"Sending: $msg"); msg }
      .toMat(BroadcastHub.sink(bufferSize = 256))(Keep.both)
      .run()

  private def socketFlow(userName: String): Flow[Message, Message, NotUsed] =
    receive(userName)
      .prepend(Source.single(UserJoined(userName)))
      .concat(Source.single(UserLeft(userName)))
      .via(Flow.fromSinkAndSource(sink, source))
      .via(send)

  private def receive(userName: String): Flow[Message, Protocol.Message, NotUsed] =
    Flow[Message]
      .collect { case TextMessage.Strict(msg) ⇒ msg }
      .map(ChatMessage(userName, _))

  private def send: Flow[Protocol.Message, Message, NotUsed] =
    Flow[Protocol.Message]
      .map(_.asJson.noSpaces)
      .map(TextMessage(_))
}
