package com.example.chat.route

import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route

class StaticFilesRoute {

  def route: Route = {
    path("") {
      getFromResource("public/index.html")
    } ~ pathPrefix(Remaining) { file =>
      getFromResource("public/" + file)
    }
  }
}
