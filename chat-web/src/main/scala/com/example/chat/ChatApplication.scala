package com.example.chat

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.server.Directives._
import akka.stream.{ActorMaterializer, Materializer}
import com.example.chat.route.{StaticFilesRoute, WebsocetRoute}
import org.slf4j.LoggerFactory

import scala.util.Failure

object ChatApplication extends App {

  private val logger = LoggerFactory.getLogger(getClass)
  private implicit val system: ActorSystem = ActorSystem("chat")
  private implicit val materializer: Materializer = ActorMaterializer()

  import materializer.executionContext

  private val httpConfig = pureconfig.loadConfigOrThrow[HttpConfig]("http")

  private val route = List(
    (new StaticFilesRoute).route,
    (new WebsocetRoute).route
  ).reduceLeft(_ ~ _)

  Http().bindAndHandle(
    handler = route,
    interface = httpConfig.interface,
    port = httpConfig.port
  ).onComplete {
    case Failure(e) =>
      logger.info("Binding failed", e)
      system.terminate()
    case _ => logger.info(s"Bound to interface ${httpConfig.interface}:${httpConfig.port}")
  }

}
