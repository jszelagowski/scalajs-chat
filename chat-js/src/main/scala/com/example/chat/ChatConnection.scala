package com.example.chat

import com.example.chat.html.Notices.prependNotice
import com.example.chat.html.{MessageForm, NameField, UserNameForm}
import com.example.chat.serialization.JsonSerialization
import com.example.chat.serialization.JsonSerialization._
import org.scalajs.dom._

import scala.concurrent.ExecutionContext.Implicits.global
import scala.scalajs.js.URIUtils._


object ChatConnection {

  def main(args: Array[String]): Unit = {
    UserNameForm.show()
    UserNameForm.onSubmit(connect)
  }

  def connect(userName: String): Unit = {

    prependNotice(s"Trying to register as $userName...")
    UserNameForm.hide()

    val chat = new WebSocket(websocketUri(userName))

    chat.onopen = { event: Event ⇒
      prependNotice(s"Connected as $userName!")
      NameField.setName(userName)
      MessageForm.show()
      MessageForm.bindSendButton(chat.send)
    }

    chat.onerror = { event: Event ⇒
      prependNotice(s"Failed: code: ${event.asInstanceOf[ErrorEvent].colno}")
      UserNameForm.show()
    }

    chat.onmessage = { event: MessageEvent ⇒
      JsonSerialization.decodeAsFuture[Protocol.Message](event.data.toString)
        .map {
          case Protocol.ChatMessage(sender, message) ⇒ prependNotice(s"$sender says: $message")
          case Protocol.UserJoined(user) ⇒ prependNotice(s"$user joined chat")
          case Protocol.UserLeft(user) ⇒ prependNotice(s"$user left chat")
        }
        .recover { case e => println(e) }
    }

    chat.onclose = { event: Event ⇒
      prependNotice("Connection closed, you can try to reconnect")
      MessageForm.hide()
      UserNameForm.show()
    }
  }

  private def websocketUri(userName: String): String = {
    val wsProtocol = if (window.location.protocol == "https:") "wss" else "ws"
    s"$wsProtocol://${window.location.host}/${Paths.ChatSocket}?name=${encodeURI(userName)}"
  }
}
