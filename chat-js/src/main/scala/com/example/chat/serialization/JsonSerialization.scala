package com.example.chat.serialization

import io.circe.generic.AutoDerivation
import io.circe.java8.time.TimeInstances
import io.circe.{Decoder, Encoder}

import scala.concurrent.Future

object JsonSerialization extends AutoDerivation with TimeInstances {

  import io.circe.parser.decode
  import io.circe.syntax._

  def decodeAsFuture[T](json: String)(implicit decoder: Decoder[T]): Future[T] =
    decode[T](json) match {
      case Right(e) => Future.successful(e)
      case Left(error) => Future.failed(error)
    }

  def toJson[T](obj: T)(implicit encoder: Encoder[T]): String =
    obj.asJson.noSpaces
}
