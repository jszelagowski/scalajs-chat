package com.example.chat.html

import org.scalajs.dom.document

object Notices {
  private val noticesArea = document.getElementById("notices")

  def prependNotice(text: String): Unit =
    noticesArea.insertBefore(li(text), noticesArea.firstChild)

  private def li(msg: String) = {
    val paragraph = document.createElement("li")
    paragraph.innerHTML = msg
    paragraph
  }
}
