package com.example.chat.html

import org.scalajs.dom.document

object NameField {

  private val messageField = document.getElementById("name-placeholder")

  def setName(name: String): Unit =
    messageField.innerHTML = name
}
