package com.example.chat.html

import org.scalajs.dom.raw.{HTMLFormElement, HTMLInputElement}
import org.scalajs.dom.{Event, document}

object MessageForm {

  private val messageForm = document.getElementById("message-form").asInstanceOf[HTMLFormElement]
  private val messageField = document.getElementById("message").asInstanceOf[HTMLInputElement]

  def hide(): Unit = messageForm.style.display = "none"

  def show(): Unit = messageForm.style.display = "block"

  def bindSendButton(send: String => Unit): Unit = {
    messageField.focus()
    messageForm.onsubmit = { event: Event ⇒
      event.preventDefault()
      send(messageField.value)
      messageField.value = ""
      messageField.focus()
    }
  }

}
