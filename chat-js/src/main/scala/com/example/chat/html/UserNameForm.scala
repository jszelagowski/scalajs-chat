package com.example.chat.html

import org.scalajs.dom.raw.{HTMLFormElement, HTMLInputElement}
import org.scalajs.dom.{Event, document}

object UserNameForm {

  private val userNameForm = document.getElementById("username-form").asInstanceOf[HTMLFormElement]
  private val userInput = document.getElementById("username-input").asInstanceOf[HTMLInputElement]

  def hide(): Unit = userNameForm.style.display = "none"

  def show(): Unit = userNameForm.style.display = "block"

  def onSubmit(action: String => Unit): Unit = {
    userNameForm.onsubmit = { event: Event =>
      event.preventDefault()
      action(userInput.value)
      userInput.value = ""
    }
  }

}
