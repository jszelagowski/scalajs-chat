import org.scalajs.sbtplugin.ScalaJSPlugin.autoImport._
import sbt._

object Dependencies {

  private val TestScopes = "test"

  private val akkaVersion = "2.5.13"
  private val akkaHttpVersion = "10.1.3"
  private val akkaStreamContribVersion = "0.9"
  val akka = Seq(
    "com.typesafe.akka" %% "akka-actor" % akkaVersion,
    "com.typesafe.akka" %% "akka-stream" % akkaVersion,
    "com.typesafe.akka" %% "akka-http" % akkaHttpVersion,
    "com.typesafe.akka" %% "akka-slf4j" % akkaVersion,

    "com.typesafe.akka" %% "akka-testkit" % akkaVersion % TestScopes,
    "com.typesafe.akka" %% "akka-stream-testkit" % akkaVersion % TestScopes,
    "com.typesafe.akka" %% "akka-http-testkit" % akkaHttpVersion % TestScopes
  )

  private val circeVersion = "0.9.3"
  val circe: Seq[ModuleID] = Seq(
    "io.circe" %% "circe-core",
    "io.circe" %% "circe-parser",
    "io.circe" %% "circe-generic-extras",
    "io.circe" %% "circe-shapes",
    "io.circe" %% "circe-refined"
  ).map(_ % circeVersion)
  val jsCirce = Def.setting(Seq(
    "io.circe" %%% "circe-core",
    "io.circe" %%% "circe-generic",
    "io.circe" %%% "circe-java8",
    "io.circe" %%% "circe-parser"
  ).map(_ % circeVersion))

  private val slf4jVersion = "1.7.25"
  private val logbackVersion = "1.2.3"
  val logging: Seq[ModuleID] = Seq(
    "org.slf4j" % "slf4j-api" % slf4jVersion,
    "ch.qos.logback" % "logback-classic" % logbackVersion
  )

  private val pureConfigVersion = "0.9.1"
  val pureConfig: Seq[ModuleID] = Seq(
    "com.github.pureconfig" %% "pureconfig"
  ).map(_ % pureConfigVersion)

  val jQueryVersion = "3.2.1"
  val jQuery = Seq(
    "org.webjars" % "jquery" % jQueryVersion
  )

  private val scalaJsDomVersion = "0.9.2"
  val scalaJs = Def.setting(Seq(
    "org.scala-js" %%% "scalajs-dom" % scalaJsDomVersion
  ))

  private val easyMockVersion = "3.6"
  private val scalaTestVersion = "3.0.5"
  val testing: Seq[ModuleID] = Seq(
    "org.easymock" % "easymock" % easyMockVersion,
    "org.scalatest" %% "scalatest" % scalaTestVersion
  ).map(_ % TestScopes)
}
