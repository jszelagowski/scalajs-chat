package com.example.chat

object Protocol {

  sealed trait Message

  case class ChatMessage(sender: String, message: String) extends Message

  case class UserJoined(user: String) extends Message

  case class UserLeft(user: String) extends Message

}