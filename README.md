# ScalaJs Chat

Simple chat utilizing scalaJS, akka streams and websockets

# Usage

## Run

```bash
sbt scalajs-chat-web/run
```
and visit [http://localhost:8877](http://localhost:8877)

# Credits
* Inspired by [https://github.com/jrudolph/akka-http-scala-js-websocket-chat](https://github.com/jrudolph/akka-http-scala-js-websocket-chat)
