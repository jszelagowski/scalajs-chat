name := "scalajs-chat"
version := "0.1"
scalaVersion := "2.12.6"

lazy val chatRoot = Project(id = "scalajs-chat", base = file("."))
  .aggregate(chatShared, chatJs, chatWeb)

lazy val chatShared = Project("scalajs-chat-shared", file("chat-shared"))
  .enablePlugins(ScalaJSPlugin)

lazy val chatJs = Project("scalajs-chat-js", file("chat-js"))
  .enablePlugins(ScalaJSPlugin, ScalaJSWeb)
  .settings(
    libraryDependencies ++= Seq(
      Dependencies.scalaJs.value,
      Dependencies.jsCirce.value,
    ).flatten
  )
  .settings(
    addCompilerPlugin("org.scalamacros" % "paradise" % "2.1.0" cross CrossVersion.full),
    scalaJSUseMainModuleInitializer := true,
  )
  .dependsOn(chatShared)

lazy val chatWeb = Project("scalajs-chat-web", file("chat-web"))
  .enablePlugins(SbtWeb)
  .settings(
    scalaJSProjects := Seq(chatJs),
    pipelineStages in Assets := Seq(scalaJSPipeline),
    // triggers scalaJSPipeline when using compile or continuous compilation
    compile in Compile := ((compile in Compile) dependsOn scalaJSPipeline).value,
    WebKeys.packagePrefix in Assets := "public/",
    (managedClasspath in Runtime) += (packageBin in Assets).value,

    libraryDependencies ++= Seq(
      Dependencies.akka,
      Dependencies.circe,
      Dependencies.logging,
      Dependencies.pureConfig,
      Dependencies.testing,
    ).flatten
  )
  .dependsOn(chatShared)